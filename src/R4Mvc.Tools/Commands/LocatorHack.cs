﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Locator;

namespace R4Mvc.Tools.Commands
{
    public static class LocatorHack
    {
        public static VisualStudioInstance VSInstance;

        public static VisualStudioInstance InitialiseMSBuild()
        {
            if (VSInstance != null)
            {
                return VSInstance;
            }

            var instances = MSBuildLocator.QueryVisualStudioInstances().ToArray();
            if (instances.Length == 0)
                Console.WriteLine("No Visual Studio instances found. The code generation might fail");

            var vsInstanceIndex = 0;
            if (vsInstanceIndex < 0 || vsInstanceIndex > instances.Length)
            {
                Console.WriteLine("Invalid VS instance. Falling back to the default one");
                vsInstanceIndex = 0;
            }

            VisualStudioInstance instance;
            if (vsInstanceIndex > 0)
            {
                // Register the selected vs instance. This will cause MSBuildWorkspace to use the MSBuild installed in that instance.
                // Note: This has to be registered *before* creating MSBuildWorkspace. Otherwise, the MEF composition used by MSBuildWorkspace will fail to compose.
                instance = instances[vsInstanceIndex - 1];
                MSBuildLocator.RegisterInstance(instance);
            }
            else
            {
                // Use the default vs instance and it's MSBuild
                instance = MSBuildLocator.RegisterDefaults();
            }

            AssemblyLoadContext.Default.Resolving += (assemblyLoadContext, assemblyName) =>
            {
                var path = Path.Combine(instance.MSBuildPath, assemblyName.Name + ".dll");
                if (File.Exists(path))
                {
                    return assemblyLoadContext.LoadFromAssemblyPath(path);
                }

                return null;
            };

            VSInstance = instance;

            return instance;
        }
    }
}
